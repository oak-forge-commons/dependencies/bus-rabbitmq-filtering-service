FROM adoptopenjdk/openjdk11:alpine

RUN mkdir /certs

COPY src/main/resources/client_key.p12 /certs
COPY src/main/resources/core.p12 /certs
COPY src/main/resources/server_store.jks /certs

COPY /build/libs/bus-rabbitmq-filtering-service-0.1-POC.jar /
COPY init.sh /

RUN ["chmod", "+x", "/init.sh"]
EXPOSE 8481

ENTRYPOINT ["/init.sh"]
