package oak.forge.commons.rabbitmq.filtering.service

import oak.forge.commons.rabbitmq.client.RabbitBusClient
import spock.lang.Specification

class RabbitMqFilterTest extends Specification {

    RabbitBusClient filteringService;

    void setup() {
        filteringService = Mock()
    }

    def "should_post_message_after_filtering"() {
        given:
        RabbitMqFilter rabbitMqFilter = new RabbitMqFilter(filteringService)
        TestMessage testMessage = new TestMessage()

        when:
        rabbitMqFilter.handleMessage(testMessage)

        then:
        1 * filteringService.post(testMessage)
    }

}
